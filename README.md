# README #

### What is this repository for? ###

This is for a technical test I did for Urgent Technology, to hold everything needed for the web application I created as a solution.

### How do I get set up? ###

I created a new Web Application in Visual Studio 2012, and it created a lot more default content than I needed!  I have deleted swathes of stuff, but there is still a lot of stuff left that is not strictly needed for the solution.

The important bits (the bits I wrote) are Tickets.aspx and Tickets.aspx.cs.  The application came with a Master page which I have left in, as they can be useful, and it is easier to start off with one and take it out later, than to start without one and have to add it in later.  Also there is some styling in a stylesheet, and some jQuery, which I have left in.

###Assumptions ###

It is clear from the specification that if you sell 300 tickets you should have 6 stewards, and if you sell 350 you should have 7; but how many stewards should you have if you have not sold a number of tickets that's a multiple of 50; should you round up, down or off?  I made the assumption that each steward could be responsible for a maximum of 50 people, so as soon as you had 51 tickets sold you would need an extra steward.   Similarly, for up to 5 stewards you would have 1 supervisor, but for 6-10 stewards you would need 2 supervisors, and so on.  

I have assumed that the football stadium capacity is limited to 99,999, so that I can validate the input amount is within a range.  This limit could be enlarged if necessary, it has no impact on the calculation.

### Recommendations on future enhancements ###

It would be possible to use AJAX so that only the output part of the page was refreshed when the result was calculated, which would improve the look of the page.

The limits of 50 tickets per steward and 5 stewards per supervisor have been hard coded; I try and avoid this and would prefer to have these constants read in from somewhere.  If they rarely change they could be in the web.config, if they change more often, a web page could be provided where the values could be maintained.

It would be possible to have multiple levels of staff, with for instance one Manager for every 10 Supervisors, with multiple levels of Managers so, at the top level there would be one person, with a hierarchy below them with as many levels as necessary so that no-one has to manage too many people.

### Who do I talk to? ###

Created by Greg Parker, gregparker2@yahoo.co.uk