﻿using System;
using System.Web.UI;

namespace Urgent
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void butTickets_Click(object sender, EventArgs e)
        {
            int tickets;
            
            if(tbTickets.Text == "")
                tickets = 0;
            else
                tickets = Convert.ToInt32(tbTickets.Text);

            // Divide by 50 (as a double), add 0.99, then cast the result to int, so we round stewards up to the next whole number.
            int stwTot = (int)(tickets / 50.0 + 0.99);

            // Round supervisors up.
            int supTot = (int)(stwTot / 5.0 + 0.9);

            int stwAdd, supAdd;

            if(stwTot <= 5)
            {
                stwTot = 5;
                stwAdd = 0;
            }
            else
               stwAdd = stwTot - 5;

            if (supTot <= 1)
            {
                supTot = 1;
                supAdd = 0;
            }
            else
                supAdd = supTot - 1;

            lblStwTot.Text = stwTot.ToString();
            lblSupTot.Text = supTot.ToString();
            lblStwAdd.Text = stwAdd.ToString();
            lblSupAdd.Text = supAdd.ToString();
        }
    }
}