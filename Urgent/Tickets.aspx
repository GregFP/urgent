﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tickets.aspx.cs" Inherits="Urgent._Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>Staffing Requirements</h3>
    <br />
    <asp:Label ID="lblTickets" runat="server" >Tickets Sold:</asp:Label> &nbsp; <asp:TextBox ID="tbTickets" runat="server" MaxLength="5" Width="49px"></asp:TextBox>
    &nbsp;<asp:RangeValidator ID="rvTickets" runat="server" ErrorMessage="RangeValidator" MaximumValue="99999" MinimumValue="0" Type="Integer" Text="Tickets sold must be between 0 and 99999" ControlToValidate="tbTickets" ForeColor="Red"></asp:RangeValidator><br />
<asp:Button ID="butTickets" runat="server" Text="Calculate" OnClick="butTickets_Click" TabIndex="1" />

<table style="width: 100%;">      
    <tr>
        <td>&nbsp;</td>
        <td>Stewards (S1)</td>
        <td>Supervisors (S3)</td>
    </tr>
    <tr>    
        <td>Total</td>
        <td><asp:Label ID="lblStwTot" runat="server" Text=""></asp:Label></td>
        <td><asp:Label ID="lblSupTot" runat="server" Text=""></asp:Label></td>
    </tr>
    <tr>
        <td>of which Contract:</td>
        <td>5</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Additional to Contract:</td>
        <td><asp:Label ID="lblStwAdd" runat="server" Text=""></asp:Label></td>
        <td><asp:Label ID="lblSupAdd" runat="server" Text=""></asp:Label></td>
    </tr>
</table>

</asp:Content>
